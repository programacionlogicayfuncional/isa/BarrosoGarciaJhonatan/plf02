(ns plf02.core)

;Predicados

;associative?
(defn funcion-associative?-1
  [coll]
  (associative? coll))

(defn funcion-associative?-2
  [coll]
  (associative? coll))

(defn funcion-associative?-3
  [coll]
  (associative? coll))

(funcion-associative?-1 [20 14 38])
(funcion-associative?-2 {:a 10 :b 20 :c 40})
(funcion-associative?-3 '(15 14 13 12 10 1))


;boolean?
(defn funcion-boolean?-1
  [x]
  (boolean? x))

(defn funcion-boolean?-2
  [x]
  (boolean? x))

(defn funcion-boolean?-3
  [x]
  (boolean? x))

(funcion-boolean?-1 false)
(funcion-boolean?-2 true)
(funcion-boolean?-3 0)


;char?
(defn funcion-char?-1
  [x]
  (char? x))

(defn funcion-char?-2
  [x]
  (char? x))

(defn funcion-char?-3
  [x]
  (char? x))

(funcion-char?-1 5)
(funcion-char?-2 (first "Alonso"))
(funcion-char?-3 \a)


;coll?
(defn funcion-coll?-1
  [x]
  (coll? x))

(defn funcion-coll?-2
  [x]
  (coll? x))

(defn funcion-coll?-3
  [x]
  (coll? x))

(funcion-coll?-1 [20 14 38])
(funcion-coll?-2 {:a 10 :b 20 :c 40})
(funcion-coll?-3 (seq "abcdefghij"))


;decimal?
(defn funcion-decimal?-1
  [n]
  (decimal? n))

(defn funcion-decimal?-2
  [n]
  (decimal? n))

(defn funcion-decimal?-3
  [n]
  (decimal? n))

(funcion-decimal?-1 1.00000)
(funcion-decimal?-2 1.5M)
(funcion-decimal?-3 2.9M)


;double?
(defn funcion-double?-1
  [x]
  (double? x))

(defn funcion-double?-2
  [x]
  (double? x))

(defn funcion-double?-3
  [x]
  (double? x))

(funcion-double?-1 1.0)
(funcion-double?-2 151.011)
(funcion-double?-3 75)


;float?
(defn funcion-float?-1
  [n]
  (float? n))

(defn funcion-float?-2
  [n]
  (float? n))

(defn funcion-float?-3
  [n]
  (float? n))

(funcion-float?-1 1.00000)
(funcion-float?-2 0.9999)
(funcion-float?-3 (* 1 0.00001))


;ident?
(defn funcion-ident?-1
  [x]
  (ident? x))

(defn funcion-ident?-2
  [x]
  (ident? x))

(defn funcion-ident?-3
  [x]
  (ident? x))

(funcion-ident?-1 :a)
(funcion-ident?-2 'abeja)
(funcion-ident?-3 "hola")


;indexed?
(defn funcion-indexed?-1
  [coll]
  (indexed? coll))

(defn funcion-indexed?-2
  [coll]
  (indexed? coll))

(defn funcion-indexed?-3
  [coll]
  (indexed? coll))

(funcion-indexed?-1 [20 14 38 1 7 8 9])
(funcion-indexed?-2 {:a 10 :b 20 :c 40})
(funcion-indexed?-3 '(15 14 13 12 10 1))


;int?
(defn funcion-int?-1
  [x]
  (int? x))

(defn funcion-int?-2
  [x]
  (int? x))

(defn funcion-int?-3
  [x]
  (int? x))

(funcion-int?-1 15987)
(funcion-int?-2 151.011)
(funcion-int?-3 (java.lang.Integer. 42))


;integer?
(defn funcion-integer?-1
  [n]
  (integer? n))

(defn funcion-integer?-2
  [n]
  (integer? n))

(defn funcion-integer?-3
  [n]
  (integer? n))

(funcion-integer?-1 1089)
(funcion-integer?-2 2.0)
(funcion-integer?-3 (java.lang.Integer. 142))


;keyword?
(defn funcion-keyword?-1
  [x]
  (keyword? x))

(defn funcion-keyword?-2
  [x]
  (keyword? x))

(defn funcion-keyword?-3
  [x]
  (keyword? x))

(funcion-keyword?-1 :araña)
(funcion-keyword?-2 :abd..)
(funcion-keyword?-3 :1.0-)


;list?
(defn funcion-list?-1
  [x]
  (list? x))

(defn funcion-list?-2
  [x]
  (list? x))

(defn funcion-list?-3
  [x]
  (list? x))

(funcion-list?-1 (list 14 38 1 7 8 9))
(funcion-list?-2 {:a 10 :b 20 :c 40})
(funcion-list?-3 '(15 14 13 12 10 1))


;map-entry?
(defn funcion-map-entry?-1
  [x]
  (map-entry? x))

(defn funcion-map-entry?-2
  [x]
  (map-entry? x))

(defn funcion-map-entry?-3
  [x]
  (map-entry? x))

(funcion-map-entry?-1 (first {:a 1 :b 2 :c 7}))
(funcion-map-entry?-2 (second {:a 10 :b 15 :c 20}))
(funcion-map-entry?-3 (last {:a \A :b \B :c \C}))


;map?
(defn funcion-map?-1
  [x]
  (map? x))

(defn funcion-map?-2
  [x]
  (map? x))

(defn funcion-map?-3
  [x]
  (map? x))

(funcion-map?-1 {:a 1 :b 2 :c 7})
(funcion-map?-2 (hash-map :a 10 :b 15 :c 20))
(funcion-map?-3 #{10 20 30 40 70})


;nat-int?
(defn funcion-nat-int?-1
  [x]
  (nat-int? x))

(defn funcion-nat-int?-2
  [x]
  (nat-int? x))

(defn funcion-nat-int?-3
  [x]
  (nat-int? x))

(funcion-nat-int?-1 -120)
(funcion-nat-int?-2 10)
(funcion-nat-int?-3 0.0)


;number?
(defn funcion-number?-1
  [x]
  (number? x))

(defn funcion-number?-2
  [x]
  (number? x))

(defn funcion-number?-3
  [x]
  (number? x))

(funcion-number?-1 -11)
(funcion-number?-2 100)
(funcion-number?-3 10.0)


;pos-int?
(defn funcion-pos-int?-1
  [x]
  (pos-int? x))

(defn funcion-pos-int?-2
  [x]
  (pos-int? x))

(defn funcion-pos-int?-3
  [x]
  (pos-int? x))

(funcion-pos-int?-1 -11)
(funcion-pos-int?-2 9223372036854)
(funcion-pos-int?-3 10)


;ratio?
(defn funcion-ratio?-1
  [n]
  (ratio? n))

(defn funcion-ratio?-2
  [n]
  (ratio? n))

(defn funcion-ratio?-3
  [n]
  (ratio? n))

(funcion-ratio?-1 11/5)
(funcion-ratio?-2 0.5)
(funcion-ratio?-3 1/2)


;rational?
(defn funcion-rational?-1
   [n] 
  (rational? n))

(defn funcion-rational?-2
  [n]
  (rational? n))

(defn funcion-rational?-3
  [n]
  (rational? n))

(funcion-rational?-1 11/5)
(funcion-rational?-2 14/2)
(funcion-rational?-3 10)


;seq?
(defn funcion-seq?-1
  [x]
  (seq? x))

(defn funcion-seq?-2
  [x]
  (seq? x))

(defn funcion-seq?-3
  [x]
  (seq? x))

(funcion-seq?-1 (seq '(1 2 3 4 5 9)))
(funcion-seq?-2 (range 1 5))
(funcion-seq?-3 [7 4 7 1 2])


;seqable?
(defn funcion-seqable?-1
  [x]
  (seqable? x))

(defn funcion-seqable?-2
  [x]
  (seqable? x))

(defn funcion-seqable?-3
  [x]
  (seqable? x))

(funcion-seqable?-1 '(1 2 3 4 5 9))
(funcion-seqable?-2 (range 1 5))
(funcion-seqable?-3 [7 4 7 1 2])


;sequential?
(defn funcion-sequential?-1
  [coll]
  (sequential? coll))

(defn funcion-sequential?-2
  [coll]
  (sequential? coll))

(defn funcion-sequential?-3
  [coll]
  (sequential? coll))

(funcion-sequential?-1 [20 14 38])
(funcion-sequential?-2 {:a 10 :b 20 :c 40})
(funcion-sequential?-3 '(15 14 13 12 10 1))


;set?
(defn funcion-set?-1
  [x]
  (set? x))

(defn funcion-set?-2
  [x]
  (set? x))

(defn funcion-set?-3
  [x]
  (set? x))

(funcion-set?-1 #{'(1 2 3 49) 5})
(funcion-set?-2 (hash-set (range 1 5) (range 6 10)))
(funcion-set?-3 [7 4 7 1 2])


;some?
(defn funcion-some?-1
  [x]
  (some? x))

(defn funcion-some?-2
  [x]
  (some? x))

(defn funcion-some?-3
  [x]
  (some? x))

(funcion-some?-1 [20 14 38 1 7 8 9])
(funcion-some?-2 "hola")
(funcion-some?-3 nil)


;string?
(defn funcion-string?-1
  [x]
  (string? x))

(defn funcion-string?-2
  [x]
  (string? x))

(defn funcion-string?-3
  [x]
  (string? x))

(funcion-string?-1 "Hola, buen día")
(funcion-string?-2 "")
(funcion-string?-3 nil)


;symbol?
(defn funcion-symbol?-1
  [x]
  (symbol? x))

(defn funcion-symbol?-2
  [x]
  (symbol? x))

(defn funcion-symbol?-3
  [x]
  (symbol? x))

(funcion-symbol?-1 'b)
(funcion-symbol?-2 "")
(funcion-symbol?-3 :a)


;vector?
(defn funcion-vector?-1
  [x]
  (vector? x))

(defn funcion-vector?-2
  [x]
  (vector? x))

(defn funcion-vector?-3
  [x]
  (vector? x))

(funcion-vector?-1 [-1 -2 -3 -4 -5 -8 -10 -12])
(funcion-vector?-2 (vector (range 5 12)))
(funcion-vector?-3 '(1 9 78 55 222 2 4 7 8 5))


;drop
(defn funcion-drop-1
  [n]
  (drop n))

(defn funcion-drop-2
  [n coll]
  (drop n coll))

(defn funcion-drop-3
  [n coll]
  (drop n coll))

(funcion-drop-1 [20 14 38])
(funcion-drop-2 2 (range 5 20))
(funcion-drop-3 1 (map (fn[x](* x x)) [2 4 6 8 10]))


;drop-last
(defn funcion-drop-last-1
  [coll]
  (drop-last coll))

(defn funcion-drop-last-2
  [n coll]
  (drop-last n coll))

(defn funcion-drop-last-3
  [n coll]
  (drop-last n coll))

(funcion-drop-last-1 [20 14 38])
(funcion-drop-last-2 5 (range 1 20))
(funcion-drop-last-3 3 (map (fn [x] (* x x)) [2 4 6 8 10 11 12 13 14 15 16]))


;drop-while
(defn funcion-drop-while-1
  [pred coll]
  (drop-while pred coll))

(defn funcion-drop-while-2
  [pred coll]
  (drop-while pred coll))

(defn funcion-drop-while-3
  [pred coll]
  (drop-while pred coll))

(funcion-drop-while-1 number? [20 14 38 9 [10 10] \a \b \c])
(funcion-drop-while-2 odd? (range 1 20))
(funcion-drop-while-3 even? (map (fn [x] (* x x)) [2 4 6 8 10 11 12 13 14 15 16]))


;every?
(defn funcion-every?-1
  [pred coll]
  (every? pred coll))

(defn funcion-every?-2
  [pred coll]
  (every? pred coll))

(defn funcion-every?-3
  [pred coll]
  (every? pred coll))

(funcion-every?-1 number? [20 14 38 9 [10 10] \a \b \c])
(funcion-every?-2 pos? (range 1 20))
(funcion-every?-3 even? (map (fn [x] (* 2 x)) [2 4 6 8 10 11 12 13 14 15 16]))


;filterv
(defn funcion-filterv-1
  [pred coll]
  (filterv pred coll))

(defn funcion-filterv-2
  [pred coll]
  (filterv pred coll))

(defn funcion-filterv-3
  [pred coll]
  (filterv pred coll))

(funcion-filterv-1 number? [20 14 38 9 [10 10] \a \b \c])
(funcion-filterv-2 (fn [x](= (count x) 1))["araña" "abeja" "b" "n" "f" "lisp" "función" "l" ""])
(funcion-filterv-3 (fn [y](> y 20)) (map (fn [x] (* x x)) [2 4 6 8 10 11 12 13 14 15 16]))


;group-by
(defn funcion-group-by-1
  [f coll]
  (group-by f coll))

(defn funcion-group-by-2
  [f coll]
  (group-by f coll))

(defn funcion-group-by-3
  [f coll]
  (group-by f coll))

(funcion-group-by-1 number? [20 14 38 9 [10 10] \a \b \c '(4 7 8)])
(funcion-group-by-2 (fn [x] (= (count x) 5)) ["araña" "abeja" "b" "n" "f" "lisp" "función" "l" ""])
(funcion-group-by-3 (fn [y] (> y 40)) (map (fn [x] (* x x)) [2 4 6 8 10 11 12 13 14 15 16]))


;iterate
(defn funcion-iterate-1
  [f x]
  (take 10 (iterate f x)))

(defn funcion-iterate-2
  [f x]
  (take 10(iterate f x)))

(defn funcion-iterate-3
  [f x]
  (take 10 (iterate f x)))

(funcion-iterate-1 dec 20)
(funcion-iterate-2 (partial * 2)  1)
(funcion-iterate-3 (fn [x] (* 4 x)) 10)


;keep
(defn funcion-keep-1
  [f coll]
  (keep f coll))

(defn funcion-keep-2
  [f coll]
  (keep f coll))

(defn funcion-keep-3
  [f coll]
  (keep f coll))

(funcion-keep-1 number? [20 14 38 9 [10 10] \a \b \c '(4 7 8)])
(funcion-keep-2  #(if (odd? %) %) (range 10))
(funcion-keep-3  #(if (pos? %) %) (range -5 10))


;keep-indexed
(defn funcion-keep-indexed-1
  [f coll]
  (keep-indexed f coll))

(defn funcion-keep-indexed-2
  [f coll]
  (keep-indexed f coll))

(defn funcion-keep-indexed-3
  [f coll]
  (keep-indexed f coll))

(funcion-keep-indexed-1 (fn [id v] 
                          (if (pos? v) id)) [-9 0 29 -7 45 3 -8])

(funcion-keep-indexed-2 (fn [id v]
                          (if (neg? v) (* -1 v)))[-9 0 29 -7 45 3 -8 -10 -12 2 3 4 -7])

(funcion-keep-indexed-3 (fn [id v]
                          (if (even? v) id)) [-9 0 29 -7 45 3 -8])


;map-indexed
(defn funcion-map-indexed-1
  [f coll]
  (map-indexed f coll))

(defn funcion-map-indexed-2
  [f coll]
  (map-indexed f coll))

(defn funcion-map-indexed-3
  [f coll]
  (map-indexed f coll))

(funcion-map-indexed-1 (fn [idx itm] [idx itm]) "Alonso Garcia")
(funcion-map-indexed-2 vector "Programación funcional")
(funcion-map-indexed-3  hash-map {:nombre "Pedro" :edad 15 :primer-apellido "Garcia"})


;mapcat
(defn funcion-mapcat-1
  [f coll]
  (mapcat f coll))

(defn funcion-mapcat-2
  [f coll]
  (mapcat f coll))

(defn funcion-mapcat-3
  [f coll]
  (mapcat f coll))

(funcion-mapcat-1 reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(funcion-mapcat-2 sort [[-1 8 14 21] [8 15 40] [8 8 7]])
(funcion-mapcat-3 (fn [n]
                    [(- n 1) n (+ n 1)]) [1 2 3])


;mapv
(defn funcion-mapv-1
  [f coll]
  (mapv f coll))

(defn funcion-mapv-2
  [f c1 c2 c3]
  (mapv f c1 c2 c3))

(defn funcion-mapv-3
  [f c1 c2 c3 c4]
  (mapv f c1 c2 c3 c4))

(funcion-mapv-1 inc [-5 -1 4 7 8 9 10 15 71 98 -2 4])
(funcion-mapv-2 + [1 2 3] (iterate inc 1) [2 5 6])
(funcion-mapv-3 * [8 7 9 7] [-1 -2 5 9 -7 -9 -8 14 20] [2 5 4 7 8 9] [1 2 3 4])


;merge-with
(defn funcion-merge-with-1
  [f map map2 map3]
  (merge-with f map map2 map3))

(defn funcion-merge-with-2
  [f map1 map2]
  (merge-with f map1 map2))

(defn funcion-merge-with-3
  [f map1 map2 map3 map4 map5]
  (merge-with f map1 map2 map3 map4 map5))

(funcion-merge-with-1 * {:a 10 :b 15 :c 20}
                        {:a 1 :b 15 :c 20 :d 147} 
                        {:a 10 :b 15 :c 20 :e 245})
(funcion-merge-with-2 max {:a 30, :b 45}
                            {:a 15, :c 20})
(funcion-merge-with-3 +
                      {:a 1  :b 2}
                      {:a 9  :b 98  :c 0}
                      {:a 10 :b 100 :c 10}
                      {:a 5}
                      {:c 5  :d 42})


;not-any?
(defn funcion-not-any?-1
  [pred coll]
  (not-any? pred coll))

(defn funcion-not-any?-2
  [pred coll]
  (not-any? pred coll))

(defn funcion-not-any?-3
  [pred coll]
  (not-any? pred coll))

(funcion-not-any?-1 odd? (map (fn [x] (* 2 x)) [1 2 5 7]))
(funcion-not-any?-2 nil? [false true 80 20 70 \a \b \c])
(funcion-not-any?-3 neg? [-9 -7 -19 -21 -13 - 15 -19 ])


;not-every?
(defn funcion-not-every?-1
  [pred coll]
  (not-every? pred coll))

(defn funcion-not-every?-2
  [pred coll]
  (not-every? pred coll))

(defn funcion-not-every?-3
  [pred coll]
  (not-every? pred coll))

(funcion-not-every?-1 odd? (map (fn [x] (* 2 x)) [1 2 5 7]))
(funcion-not-every?-2 nil? [false true 80 20 70 \a \b \c nil])
(funcion-not-every?-3 odd? '(1 3 9 7 31))


;partition-by
(defn funcion-partition-by-1
  [f coll]
  (partition-by f coll))

(defn funcion-partition-by-2
  [f coll]
  (partition-by f coll))

(defn funcion-partition-by-3
  [f coll]
  (partition-by f coll))

(funcion-partition-by-1 odd? [1 1 1 2 2 3 3 7 7 7])
(funcion-partition-by-2 count ["a" "b" "ab" "ac" "c" "abc" "def" "pqr"])
(funcion-partition-by-3 pos? (range -5 10))
 

;reduce-kv
(defn funcion-reduce-kv-1 [f coll]
  (reduce-kv (fn [m k v] (assoc m k (f v))) (empty coll) coll))

(defn funcion-reduce-kv-2 [f coll]
  (reduce-kv (fn [m k v] (assoc m k (f v))) (empty coll) coll))

(defn funcion-reduce-kv-3
  [f init coll]
  (reduce-kv f init coll))

(funcion-reduce-kv-1 inc {:a 12, :b 19, :c 2})
(funcion-reduce-kv-2 dec [1 1 2 3 5 25 36 78 98 -1 -10 -15])
(funcion-reduce-kv-3 (fn [res idx itm] (assoc res (inc idx) itm)) {} ["uno" "dos" "tres", "cuatro", "cinco"])


;remove
(defn funcion-remove-1
  [pred coll]
  (remove pred coll))

(defn funcion-remove-2
  [pred coll]
  (remove pred coll))

(defn funcion-remove-3
  [pred coll]
  (remove pred coll))

(funcion-remove-1 number? [20 14 38 9 [10 10] \a \b \c])
(funcion-remove-2 even? (range 10))
(funcion-remove-3 (fn [x]
                  (= (count x) 1))
                  ["a" "aa" "b" "n" "f" "lisp" "clojure" "q" ""])


;reverse
(defn funcion-reverse-1
  [coll]
  (reverse coll))

(defn funcion-reverse-2
  [coll]
  (reverse coll))

(defn funcion-reverse-3
  [coll]
  (reverse coll))

(funcion-reverse-1 "Pedro Garcia")
(funcion-reverse-2 (range 10))
(funcion-reverse-3 (map (fn [x] (- (* 5 x) (* 1/3 x))) [1 4 2 8 7 9 1 4 10]))


;some
(defn funcion-some-1
  [pred coll]
  (some pred coll))

(defn funcion-some-2
  [pred coll]
  (some pred coll))

(defn funcion-some-3
  [pred coll]
  (some pred coll))

(funcion-some-1 {2 "two" 3 "three"} [nil 3 2])
(funcion-some-2 #{2 4 6} (range 3 10))
(funcion-some-3 even? '(1 2 3 4))


;sort-by
(defn funcion-sort-by-1
  [keyfn coll]
  (sort-by keyfn coll))

(defn funcion-sort-by-2
  [keyfn comp coll]
  (sort-by keyfn comp coll))

(defn funcion-sort-by-3
  [keyfn comp coll]
  (sort-by keyfn comp coll))

(funcion-sort-by-1 count ["aaa" "bb" "c", "d", "abcde", "e"])
(funcion-sort-by-2 val > {:a 7, :b 3, :c 5})
(funcion-sort-by-3 first > [[1 2] [2 2] [2 3]])


;split-with
(defn funcion-split-with-1
  [pred coll]
  (split-with pred coll))

(defn funcion-split-with-2
  [pred coll]
  (split-with pred coll))

(defn funcion-split-with-3
  [pred coll]
  (split-with pred coll))

(funcion-split-with-1 (partial > 10) [1 2 3 2 1])
(funcion-split-with-2 odd? (range 1 20))
(funcion-split-with-3 (partial >= 3) [1 2 3 4 5])


;take
(defn funcion-take-1
  [n coll]
  (take n coll))

(defn funcion-take-2
  [n coll]
  (take n coll))

(defn funcion-take-3
  [n coll]
  (take n coll))

(funcion-take-1 5 (range -1 10))
(funcion-take-2 3 (drop 5 (range 1 11)))
(funcion-take-3 5 (iterate inc 5))


;take-last
(defn funcion-take-last-1
  [n coll]
  (take-last n coll))

(defn funcion-take-last-2
  [n coll]
  (take-last n coll))

(defn funcion-take-last-3
  [n coll]
  (take-last n coll))

(funcion-take-last-1 2 (range -1 10))
(funcion-take-last-2 3 (drop 8 (range 1 11)))
(funcion-take-last-3 5 (map inc [8 9 10 11 14 17 15 18 25]))


;take-nth
(defn funcion-take-nth-1
  [n coll]
  (take-nth n coll))

(defn funcion-take-nth-2
  [n coll]
  (take-nth n coll))

(defn funcion-take-nth-3
  [n coll]
  (take-nth n coll))

(funcion-take-nth-1 2 (range -1 10))
(funcion-take-nth-2 2 (range 0 20))
(funcion-take-nth-3 5 (map inc [8 9 10 11 14 17 15 18 25]))


;take-while
(defn funcion-take-while-1
  [n coll]
  (take-while n coll))

(defn funcion-take-while-2
  [n coll]
  (take-while n coll))

(defn funcion-take-while-3
  [n coll]
  (take-while n coll))

(funcion-take-while-1 neg? (range -5 10))
(funcion-take-while-2 neg? [0 1 2 3])
(funcion-take-while-3 #{[1 2] [3 4]} #{[3 4]})


;update
(defn funcion-update-1
  [m k f]
  (update m k f))

(defn funcion-update-2
  [m k f x]
  (update m k f x))

(defn funcion-update-3
  [m k f]
  (update m k f))

(funcion-update-1 {:nombre "Pedro" :edad 26} :edad inc)
(funcion-update-2 {:nombre "Pedro" :edad 26} :edad + 10)
(funcion-update-3 [1 2 3] 0 inc)

;update-in
(defn funcion-update-in-1
   [m ks f]
   (update-in m ks f))

(defn funcion-update-in-2
  [m ks f arg1 arg2 arg3 arg4]
  (update-in m ks f arg1 arg2 arg3 arg4))

(defn funcion-update-in-3
  [m ks f arg1 arg2]
  (update-in m ks f arg1 arg2))

(funcion-update-in-1 [{:nombre "Alonso" :edad 26}  {:nombre "Jack" :edad 43}] [1 :edad] inc)
(funcion-update-in-2 {:1 {:value 0, :active false}, :2 {:value 0, :active false}} [:1] assoc :value 1 :active true )
(funcion-update-in-3 {:a 3} [:a] / 4 5)